import { Component, OnInit, OnDestroy } from '@angular/core';
import { ContactsService } from '../../services/contacts.service';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.css']
})
export class ContactsListComponent implements OnInit {
  contacts:Array<Object>;
  searchTerm:string;
  searchTermStream;

  constructor(private contactsService:ContactsService) {
    this.contacts = contactsService.getContacts();
  }

  ngOnInit() {
    this.searchTermStream = this.contactsService.getSearchTerm().subscribe(searchTerm => {
      console.log(searchTerm);
      this.searchTerm = searchTerm;
    })
  }

  ngOnDestroy() {
    this.searchTermStream.unsubscribe();
  }

}
